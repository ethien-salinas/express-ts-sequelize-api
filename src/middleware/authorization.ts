import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";
import { logger } from "../logger";

export const authorization = async (req: Request, res: Response, next: NextFunction) => {
  let token = req.headers.authorization
  if (!token) {
    return res.status(401).send({
      success: false,
      msg: 'missing token'
    })
  } else {
    try {
      token = token.split(' ')[1]
      const decoded = verify(token, process.env.JWT_SECRET)
      const jwtPayload = JSON.parse(JSON.stringify(decoded))
      logger.info(`[authorization] El usuario ${jwtPayload?.email} ha consultado la ruta ${req.originalUrl}`)
      next()
    } catch (err) {
      return res.status(401).send({
        success: false,
        msg: err.message
      })
    }
  }

}
