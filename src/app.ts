import cors from "cors"
import "dotenv/config"
import express, { Request, Response } from "express"
import { printHeaders } from "./middleware/header"
import { authRouter } from "./routes/auth"
import { userRouter } from "./routes/user"

const app = express()
const port: number = Number(process.env.PORT) || 3000

app.use(cors())
app.use(express.json())


app.get('/', printHeaders, (req: Request, res: Response) => {
  res.send({ message: 'Express API V1' })
})

app.use('/auth', authRouter)
app.use('/user', userRouter)

app.listen(port, () => {
  console.log(`API listening at http://localhost:${port}`)
})
