import bcrypt from "bcrypt"
import express, { Request, Response } from "express"
import { sign } from "jsonwebtoken"
import User from "../db/model/User"
import { logger } from "../logger"

export const authRouter = express.Router()

authRouter.post('/login', async (req: Request, res: Response) => {
  const { email, password } = req.body
  const user = await User.findOne({ where: { email } })
  if (user && await bcrypt.compare(password, user.password)) {
    const tokenData = {
      fullName: user.name + ' ' + user.lastname,
      email,
      isAdmin: user.isAdmin
    }
    logger.info(`[login] El usuario ${user.id} a accedido al sistema`)
    const token = sign(tokenData, process.env.JWT_SECRET, { expiresIn: 180 })
    res.json({ token })
  } else {
    logger.error(`[login] Credenciales inválidas para ${email}`)
    res.status(401).json({
      error: 'Unauthorized',
      code: 'USR101'
    })
  }
})

authRouter.post('/register', async (req: Request, res: Response) => {
  const {
    name,
    secondName,
    lastname,
    secondLastname,
    email,
    password,
    isAdmin
  } = req.body
  let userRequest = {
    name,
    secondName,
    lastname,
    secondLastname,
    email,
    password: await bcrypt.hash(password, 9),
    isAdmin
  }
  try {
    const userRegistered = await User.create(userRequest)
    userRegistered.password = undefined
    logger.info(`[register] El usuario ${JSON.stringify(userRegistered)} ha sido registrado con éxito`)
    const tokenData = {
      fullName: userRegistered.name + ' ' + userRegistered.lastname,
      email,
      isAdmin: userRegistered.isAdmin
    }
    const token = sign(tokenData, process.env.JWT_SECRET, { expiresIn: 180 })
    logger.info(`[register] Primer token para el usuario ${userRegistered.id}`)
    res.json({ token })
  } catch (error) {
    delete userRequest['password']
    logger.error(`[register] Error al tratar de registrar al usuario: ${JSON.stringify(userRequest)}, error: ${JSON.stringify(error)}`)
    res.send({
      error: `No es posible crear al usuario ${email}`,
      code: 'USR100'
    })
  }
})