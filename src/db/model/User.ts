import { Model, InferAttributes, InferCreationAttributes, DataTypes } from "sequelize"
import { sequelize } from ".."

export default class User extends Model<InferAttributes<User>, InferCreationAttributes<User>> {
    declare id: string
    declare name: string
    declare secondName?: string
    declare lastname: string
    declare secondLastname?: string
    declare email: string
    declare password: string
    declare isAdmin?: boolean
}

User.init({
    id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    secondName: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    lastname: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    secondLastname: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    isAdmin: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
    }
}, {
    timestamps: true,
    sequelize: sequelize
})
